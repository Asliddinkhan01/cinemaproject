package uz.pdp.cinema.repository.user;


//Asliddin Kenjaev, created: Mar, 14 2022 12:36 PM 

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.User;


public interface UserRepository extends JpaRepository<User, Integer> {
}
