package uz.pdp.cinema.repository.user;
//Asliddin Kenjaev, created: Mar, 14 2022 4:22 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
