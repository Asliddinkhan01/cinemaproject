package uz.pdp.cinema.repository.user;


//Asliddin Kenjaev, created: Mar, 14 2022 4:23 PM 

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.Permission;

public interface PermissionRepository extends JpaRepository<Permission,Integer> {
}
