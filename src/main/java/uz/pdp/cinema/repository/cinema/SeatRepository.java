package uz.pdp.cinema.repository.cinema;
//Asliddin Kenjaev, created: Mar, 17 2022 11:38 AM


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.Seat;

public interface SeatRepository extends JpaRepository<Seat, Integer> {
}
