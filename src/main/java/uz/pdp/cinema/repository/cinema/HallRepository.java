package uz.pdp.cinema.repository.cinema;


//Asliddin Kenjaev, created: Mar, 15 2022 11:43 PM 


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.Hall;

public interface HallRepository extends JpaRepository<Hall, Integer> {
}
