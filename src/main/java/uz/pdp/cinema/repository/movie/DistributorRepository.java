package uz.pdp.cinema.repository.movie;


//Asliddin Kenjaev, created: Mar, 15 2022 8:57 AM 


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.Distributor;

import java.util.UUID;

public interface DistributorRepository extends JpaRepository<Distributor, Integer> {

}
