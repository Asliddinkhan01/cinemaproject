package uz.pdp.cinema.repository.movie;
//Asliddin Kenjaev, created: Mar, 15 2022 10:57 PM

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.cinema.model.Genre;

public interface GenreRepository extends JpaRepository<Genre, Integer> {

}
