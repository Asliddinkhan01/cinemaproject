package uz.pdp.cinema.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;


@EqualsAndHashCode(callSuper = true)
@Entity(name = "distributors")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Distributor extends AbsEntity {
    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "text")
    private String description;


}
