package uz.pdp.cinema.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema.model.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "photos")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Photo extends AbsEntity {

    @ManyToOne
    private Movie movie;

    @OneToOne
    private Attachment attachment;
}
