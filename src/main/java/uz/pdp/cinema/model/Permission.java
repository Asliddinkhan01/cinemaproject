package uz.pdp.cinema.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema.model.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "permissions")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Permission extends AbsEntity {

    @Column(nullable = false)
    private String name;

}
