package uz.pdp.cinema.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema.model.template.AbsEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity(name = "movies")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Movie extends AbsEntity {


    @Column(nullable = false, length = 50)
    private String title;

    @Column(columnDefinition = "text")
    private String description;

    private int durationInMin;

    private double minPrice;

    @OneToOne
    private Attachment coverImg;

    @Column(nullable = false)
    private String trailerVideoUrl; // ex. youtube link

    @Column(nullable = false)
    private Date releaseDate;

    private Double budget;

    @ManyToOne
    private Distributor distributor;

    @Column(nullable = false)
    private Double distributorShareInPercentage;


    @ManyToMany
    private List<Cast> casts;


    @ManyToMany
    private List<Genre> genres;


}
