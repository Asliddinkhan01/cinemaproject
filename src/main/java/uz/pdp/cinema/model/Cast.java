package uz.pdp.cinema.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.cinema.model.enums.CastType;
import uz.pdp.cinema.model.template.AbsEntity;

import javax.persistence.*;


@EqualsAndHashCode(callSuper = true)
@Entity(name = "casts")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Cast extends AbsEntity {

    @Column(nullable = false)
    private String fullName;

    @OneToOne
    private Attachment photo;

    @Enumerated(EnumType.STRING)
    private CastType castType;


}
