package uz.pdp.cinema.service;


//Asliddin Kenjaev, created: Mar, 15 2022 10:57 PM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinema.model.Genre;
import uz.pdp.cinema.repository.movie.GenreRepository;

import java.util.List;
import java.util.Optional;

@Service
public class GenreService {

    @Autowired
    GenreRepository genreRepository;

    public List<Genre> getGenres(){
        return genreRepository.findAll();
    }

    public Optional<Genre> getGenreById(Integer id){
        return genreRepository.findById(id);
    }

    public Genre saveGenre(Genre genre){
        return genreRepository.save(genre);
    }

    public void deleteGenre(Integer id){
        genreRepository.deleteById(id);
    }
}
