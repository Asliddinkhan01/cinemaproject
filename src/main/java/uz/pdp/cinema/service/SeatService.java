package uz.pdp.cinema.service;


//Asliddin Kenjaev, created: Mar, 17 2022 11:39 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinema.model.Seat;
import uz.pdp.cinema.repository.cinema.SeatRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SeatService {
    @Autowired
    SeatRepository seatRepository;

    public List<Seat> getAllSeats(){
        return seatRepository.findAll();
    }

    public Optional<Seat> getSeatById(Integer id){
        return seatRepository.findById(id);
    }

    public Seat saveSeat(Seat seat){
        return seatRepository.save(seat);
    }

    public void deleteSeatById(Integer id){
        seatRepository.deleteById(id);
    }
}
