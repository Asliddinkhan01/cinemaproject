package uz.pdp.cinema.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinema.model.Hall;
import uz.pdp.cinema.repository.cinema.HallRepository;

import java.util.List;
import java.util.Optional;

//Asliddin Kenjaev, created: Mar, 15 2022 11:45 PM
@Service
public class HallService {

    @Autowired
    HallRepository hallRepository;


    public List<Hall> getAllHalls() {
        return hallRepository.findAll();
    }

    public Optional<Hall> getHallById(Integer id) {
        return hallRepository.findById(id);
    }

    public Hall saveHall(Hall hall) {
        return hallRepository.save(hall);
    }

    public void deleteHall(Integer id) {
        hallRepository.deleteById(id);
    }

}


