package uz.pdp.cinema.service;


//Asliddin Kenjaev, created: Mar, 15 2022 8:56 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinema.model.Distributor;
import uz.pdp.cinema.repository.movie.DistributorRepository;

import java.util.List;
import java.util.UUID;

@Service
public class DistributorService {

    @Autowired
    DistributorRepository distributorRepository;

    public List<Distributor> getDistributors(){
        return distributorRepository.findAll();
    }

    public Distributor getDistributorById(Integer id){
        return distributorRepository.getById(id);
    }

    public Distributor saveDistributor(Distributor distributor){
        return distributorRepository.save(distributor);
    }

    public void deleteDistributor(Integer id){
        distributorRepository.deleteById(id);
    }
}
