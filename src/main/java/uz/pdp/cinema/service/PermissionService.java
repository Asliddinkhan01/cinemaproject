package uz.pdp.cinema.service;


//Asliddin Kenjaev, created: Mar, 16 2022 8:21 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.cinema.model.Permission;
import uz.pdp.cinema.repository.user.PermissionRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PermissionService {
    @Autowired
    PermissionRepository permissionRepository;

    public List<Permission> getPermissions(){
        return permissionRepository.findAll();
    }

    public Optional<Permission> getPermissionById(Integer id){
        return permissionRepository.findById(id);
    }

    public Permission savePermission(Permission permission){
        return permissionRepository.save(permission);
    }

    public void deletePermission(Integer id){
        permissionRepository.deleteById(id);
    }

}
