package uz.pdp.cinema.payload;


//Asliddin Kenjaev, created: Mar, 15 2022 9:22 AM 

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApiResponse {

    private String message;

    private boolean isSuccess;

    private Object data;

}
