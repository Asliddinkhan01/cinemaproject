package uz.pdp.cinema.controller;


//Asliddin Kenjaev, created: Mar, 16 2022 8:24 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema.model.Permission;
import uz.pdp.cinema.payload.ApiResponse;
import uz.pdp.cinema.service.PermissionService;

import java.util.Locale;
import java.util.Optional;

@Controller
@RequestMapping("/api/permission")
public class PermissionController {

    @Autowired
    PermissionService permissionService;

    @GetMapping
    public HttpEntity<?> getPermissions() {
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        permissionService.getPermissions()
                )
        );
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getPermissionById(@PathVariable Integer id) {
        Optional<Permission> permissionById = permissionService.getPermissionById(id);
        return permissionById.map(permission -> ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        permission
                )
        )).orElseGet(() -> ResponseEntity.ok(
                new ApiResponse(
                        "Error",
                        false,
                        null
                )
        ));
    }

    @PostMapping()
    public ResponseEntity<?> savePermission(@RequestBody Permission permission) {
        try {
            Permission permission1 = permissionService.savePermission(permission);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            permission1
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                    new ApiResponse(
                            "Error",
                            false,
                            null
                    )
            );
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editPermission(@PathVariable Integer id, @RequestBody Permission permission) {
        Optional<Permission> editingPermission = permissionService.getPermissionById(id);
        if (editingPermission.isPresent()) {
            Permission permission1 = editingPermission.get();
            permission1.setName(permission.getName());
            permission1.setUpdatedAt(permission.getUpdatedAt());
            permission1.setUpdatedBy(permission.getUpdatedBy());
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            permissionService.savePermission(permission1)
                    )
            );
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePermission(@PathVariable Integer id) {
        permissionService.deletePermission(id);
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        "All Done"
                        )
        );
    }

}
