package uz.pdp.cinema.controller;


//Asliddin Kenjaev, created: Mar, 15 2022 8:53 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema.model.Distributor;
import uz.pdp.cinema.payload.ApiResponse;
import uz.pdp.cinema.service.DistributorService;

import java.util.UUID;

@Controller
@RequestMapping("/api/distributor")
public class DistributorController {

    @Autowired
    DistributorService distributorService;

    @GetMapping
    public HttpEntity<?> getDistributor() {
        return ResponseEntity.ok(
                new ApiResponse(
                        "success",
                        true,
                        distributorService.getDistributors())
        );
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getDistributor(@PathVariable Integer id){
        try {
            Distributor distributorById = distributorService.getDistributorById(id);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            distributorById
                    )
            );
        } catch (Exception e){
            return ResponseEntity.ok(
                    new ApiResponse(
                            "error",
                            false,
                            null
                    )
            );
        }
    }

    @PostMapping
    public HttpEntity<?> saveDistributor(@RequestBody Distributor distributor) {
        Distributor distributor1 = distributorService.saveDistributor(distributor);
        return ResponseEntity.ok(
                new ApiResponse(
                        "saved",
                        true,
                        distributor1
                ));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editDistributor(@PathVariable Integer id, @RequestBody Distributor distributor) {
        Distributor editingDistributor = distributorService.getDistributorById(id);
        editingDistributor.setName(distributor.getName());
        editingDistributor.setDescription(distributor.getDescription());
        editingDistributor.setUpdatedAt(distributor.getUpdatedAt());
        editingDistributor.setUpdatedBy(distributor.getUpdatedBy());

        return ResponseEntity.ok(
                new ApiResponse(
                        "Edited",
                        true,
                        editingDistributor
                )
        );
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteDistributor(@PathVariable Integer id) {
        try {
            distributorService.deleteDistributor(id);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "success",
                            true,
                            distributorService.getDistributorById(id)
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Error",
                            false,
                            null
                    )
            );
        }
    }
}
