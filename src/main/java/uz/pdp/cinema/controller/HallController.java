package uz.pdp.cinema.controller;


//Asliddin Kenjaev, created: Mar, 16 2022 12:03 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema.model.Hall;
import uz.pdp.cinema.payload.ApiResponse;
import uz.pdp.cinema.service.HallService;

import java.util.Optional;

@Controller
@RequestMapping("/api/hall")
public class HallController {

    @Autowired
    HallService hallService;

    @GetMapping
    public HttpEntity<?> getAllHalls() {
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        hallService.getAllHalls()
                )
        );
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getHallById(@PathVariable Integer id) {
        Optional<Hall> hallById = hallService.getHallById(id);
        return hallById.map(hall -> ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        hall
                )
        )).orElseGet(() -> ResponseEntity.ok(
                new ApiResponse(
                        "Error",
                        false,
                        null
                )
        ));
    }

    @PostMapping
    public HttpEntity<?> saveHall(@RequestBody Hall hall) {
        try {
            Hall savedHall = hallService.saveHall(hall);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            hall
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Error",
                            false,
                            null
                    )
            );
        }
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editHall(@PathVariable Integer id, @RequestBody Hall hall){
        Optional<Hall> editingHall = hallService.getHallById(id);
        if (editingHall.isPresent()) {
            Hall hall1 = editingHall.get();
            hall1.setName(hall.getName());
            hall1.setRows(hall.getRows());
            hall1.setUpdatedAt(hall.getUpdatedAt());
            hall1.setUpdatedBy(hall.getUpdatedBy());
            hall1.setVipAddFeeInPercent(hall.getVipAddFeeInPercent());
            Hall hall2 = hallService.saveHall(hall1);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            hall2
                    )
            );
        } else {
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Error",
                            false,
                            null
                    )
            );
        }
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteHall(@PathVariable Integer id){
        hallService.deleteHall(id);
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        null
                )
        );
    }
}

