package uz.pdp.cinema.controller;


//Asliddin Kenjaev, created: Mar, 17 2022 11:44 AM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema.model.Seat;
import uz.pdp.cinema.payload.ApiResponse;
import uz.pdp.cinema.service.SeatService;

import java.util.Optional;

@RestController
@RequestMapping("/api/seat")
public class SeatController {

    @Autowired
    SeatService seatService;

    @GetMapping
    public ResponseEntity<?> getAllSeats() {
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        seatService.getAllSeats()
                )
        );
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSeatById(@PathVariable Integer id) {
        Optional<Seat> seatById = seatService.getSeatById(id);

        if (seatById.isPresent()) {
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            seatById.get()
                    )
            );
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<?> saveSeat(@RequestBody Seat seat) {
        try {
            Seat seat1 = seatService.saveSeat(seat);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Success",
                            true,
                            seat1
                    )
            );
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(
                    new ApiResponse(
                            "Error",
                            false,
                            null
                    )
            );
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editSeat(@PathVariable Integer id, @RequestBody Seat seat) {
        Optional<Seat> editingSeat = seatService.getSeatById(id);
        if (editingSeat.isPresent()) {
            Seat editedSeat = editingSeat.get();
            editedSeat.setNumber(seat.getNumber());
            editedSeat.setRow(seat.getRow());
            editedSeat.setUpdatedBy(seat.getUpdatedBy());
            editedSeat.setUpdatedAt(seat.getUpdatedAt());
            editedSeat.setPriceCategory(seat.getPriceCategory());
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Succes",
                            true,
                            seatService.saveSeat(editedSeat)
                    )
            );
        }

        return ResponseEntity.badRequest().body(
                new ApiResponse(
                        "Error",
                        false,
                        null
                )
        );
    }

    @DeleteMapping("/{id}")
    public ApiResponse deleteSeatById(@PathVariable Integer id) {
        seatService.deleteSeatById(id);
        return new ApiResponse(
                "Success",
                true,
                null
        );
    }
}
