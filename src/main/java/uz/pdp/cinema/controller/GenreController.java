package uz.pdp.cinema.controller;


//Asliddin Kenjaev, created: Mar, 15 2022 11:08 PM 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.pdp.cinema.model.Genre;
import uz.pdp.cinema.payload.ApiResponse;
import uz.pdp.cinema.service.GenreService;

import java.util.Locale;
import java.util.Optional;

@Controller
@RequestMapping("/api/genre")
public class GenreController {

    @Autowired
    GenreService genreService;

    @GetMapping
    public HttpEntity<?> getGenres() {
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        genreService.getGenres()
                )
        );
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getGenreById(@PathVariable Integer id) {
        Optional<Genre> genreById = genreService.getGenreById(id);
        return genreById.map(genre -> ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        genre
                )
        )).orElseGet(() -> ResponseEntity.ok(
                new ApiResponse(
                        "Not Found",
                        false,
                        null
                )
        ));
    }

    @PostMapping
    public HttpEntity<?> saveGenre(@RequestBody Genre genre) {
        return ResponseEntity.ok(
                new ApiResponse(
                        "Success",
                        true,
                        genreService.saveGenre(genre)
                )
        );
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editGenre(@PathVariable Integer id, @RequestBody Genre genre) {
        Optional<Genre> editingGenre = genreService.getGenreById(id);
        if (editingGenre.isPresent()) {
            Genre genre1 = editingGenre.get();
            genre1.setName(genre.getName());
            genre1.setDescription(genre.getDescription());
            genre1.setCreatedAt(genre.getCreatedAt());
            genre1.setCreatedBy(genre.getCreatedBy());
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Saved successfully",
                            true,
                            genreService.saveGenre(genre1)
                    )
            );
        } else {
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Not found",
                            false,
                            null
                    )
            );
        }

    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteGenre(@PathVariable Integer id){
        try {
            genreService.deleteGenre(id);
            return ResponseEntity.ok(
                    new ApiResponse(
                            "success",
                            true,
                            null
                    )
            );
        } catch (Exception e){
            return ResponseEntity.ok(
                    new ApiResponse(
                            "Error",
                            false,
                            null
                    )
            );
        }
    }

}
